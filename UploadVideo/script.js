$(document).ready(function(){

  //thumbnail
  $(document).on('change', '#ad_video', function(){
    var file = event.target.files[0];
    var fileReader = new FileReader();
    if (file.type.match('image')) {
      fileReader.onload = function() {
        var img = document.createElement('img');
        img.src = fileReader.result;
        document.getElementsByTagName('div')[0].appendChild(img);
      };
      fileReader.readAsDataURL(file);
    } else {
      fileReader.onload = function() {
        var blob = new Blob([fileReader.result], {type: file.type});
        var url = URL.createObjectURL(blob);
        var video = document.createElement('video');
        var timeupdate = function() {
          if (snapImage()) {
            video.removeEventListener('timeupdate', timeupdate);
            video.pause();
          }
        };
        video.addEventListener('loadeddata', function() {
          if (snapImage()) {
            video.removeEventListener('timeupdate', timeupdate);
          }
        });
        var snapImage = function() {
          var canvas = document.createElement('canvas');
          canvas.width = video.videoWidth;
          canvas.height = video.videoHeight;
          canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
          var image = canvas.toDataURL();
          var success = image.length > 100000;
          if (success) {
            var img = document.createElement('img');
            img.src = image;
            document.getElementsByTagName('div')[0].appendChild(img);
            URL.revokeObjectURL(url);
          }
          return success;
        };
        video.addEventListener('timeupdate', timeupdate);
        video.preload = 'metadata';
        video.src = url;
        // Load video in Safari / IE11
        video.muted = true;
        video.playsInline = true;
        video.play();
      };
      fileReader.readAsArrayBuffer(file);
    }
  });    

  AWS.config = new AWS.Config();
  AWS.config.accessKeyId = "AKIAIWDYUGA5XOAWBU7Q";
  AWS.config.secretAccessKey = "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";
  AWS.config.region = "us-west-2";
  var BucketName = "syn-ai-aaoi";
  
  var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {Bucket: BucketName}
  });
  //upload      
  $(document).on('click', '#upload_button', function(){
    
    var fileChooser = document.getElementById('ad_video');
    //var button = document.getElementById('upload-button');
    var results = document.getElementById('list_videos');

    var file = fileChooser.files[0];

    if (file) {
        
        results.innerHTML = '';

        var videoKey = encodeURIComponent('videos') + '/' + file.name;
        s3.upload({
          Key: videoKey,
          Body: file,
          ACL: 'public-read'
        }, function(err, data) {
          if (err) {
            return alert('There was an error uploading your video: ', err.message);
          }
          alert('Successfully uploaded video.');
          listObjs();
        });

    } else {
        results.innerHTML = 'Nothing to upload.';
    }
  });

  //list
  $(document).on('click', '#list_button', function(){
    listObjs();
  });

  function listObjs() {
    var prefix = BucketName + '/' + 'videos';
    s3.listObjects({
      Bucket:BucketName,
      Delimiter: '/',
      Prefix: 'videos/'
    },function(err, data) {
        if (err) {
            document.getElementById('list_videos').innerHTML = 'ERROR: ' + err;
        } else {
            //var objKeys = "<ul class='list-group'>";
            var objKeys = "<div class='container'><form>";
            var prefix = 0;
            var flag = true;
            data.Contents.forEach(function(obj) {
              if(flag){
                prefix = obj.Key.length
                flag = false
              }
              var name_video = obj.Key.substring(prefix)
              objKeys += "<div class='radio'><label><input type='radio' name='video_content' value='"+name_video+"'> " + name_video + "</label></div>"; 
              //objKeys += "<li class='list-group-item'>" + obj.Key.substring(prefix) + "</li>"; 
            });
            objKeys += "</form></div>";
            document.getElementById('list_videos').innerHTML = objKeys;
        }
    });
  }

  $(document).on('click', '#scene_button', function(){
    var video_content  = $("input:radio[name='video_content']:checked").val();
    var res = video_content.split(".");
    window.open('http://localhost:1227/SceneSelection/main.html?videoName='+res[0],'_blank')
  });

});