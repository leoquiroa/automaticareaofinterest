// first we need Konva core things: stage and layer
var width = window.innerWidth;
var height = window.innerHeight - 25;
var stage = new Konva.Stage({
  container: 'container',
  width: width,
  height: height
});
var layer = new Konva.Layer();
stage.add(layer);

// then we are going to draw into special canvas element
var canvas = document.createElement('canvas');
canvas.width = 1280;
canvas.height = 720;

// created canvas we can add to layer as "Konva.Image" element
var image = new Konva.Image({
  image: canvas
  ,x: 0
  ,y: 0
  ,stroke: 'green'
  ,shadowBlur: 5
  ,opacity: 0.75
  //width: 1000,
  //height: 500
});
layer.add(image);
stage.draw();

// Good. Now we need to get access to context element
var context = canvas.getContext('2d');
context.strokeStyle = '#a4f442';
context.lineJoin = 'round';
context.lineWidth = 10;

var isPaint = false;
var lastPointerPosition;
var mode = 'brush';

// now we need to bind some events
// we need to start drawing on mousedown
// and stop drawing on mouseup
image.on('mousedown touchstart', function() {
  isPaint = true;
  lastPointerPosition = stage.getPointerPosition();
});

// will it be better to listen move/end events on the window?

stage.addEventListener('mouseup touchend', function() {
    isPaint = false;
});

// and core function - drawing
Dots = []
stage.addEventListener('mousemove touchmove', function() {
  if (!isPaint) {
    return;
  }

  if (mode === 'brush') {
    context.globalCompositeOperation = 'source-over';
  }
  if (mode === 'eraser') {
    context.globalCompositeOperation = 'destination-out';
  }
  context.beginPath();

  var localPos = {
    x: lastPointerPosition.x - image.x(),
    y: lastPointerPosition.y - image.y()
  };
  context.moveTo(localPos.x, localPos.y);
  var pos = stage.getPointerPosition();
  localPos = {
    x: pos.x - image.x(),
    y: pos.y - image.y()
  };

  Dots.push({
    x: localPos.x
    ,y: localPos.y
  });
  
  context.lineTo(localPos.x, localPos.y);
  context.closePath();
  context.stroke();

  lastPointerPosition = pos;
  layer.batchDraw();
});

function getBorders(data, axis, type) {
  if(axis=='Y'){
    if(type=='Min'){
      return data.reduce((min, p) => p.y < min ? p.y : min, data[0].y);  
    }else if(type=='Max'){
      return data.reduce((max, p) => p.y > max ? p.y : max, data[0].y);  
    }
  }else if(axis=='X'){
    if(type=='Min'){
      return data.reduce((min, p) => p.x < min ? p.x : min, data[0].x);  
    }else if(type=='Max'){
      return data.reduce((max, p) => p.x > max ? p.x : max, data[0].x);  
    }
  } 
}

$(document).ready(function(){
  var idScene = $.urlParam('idScene')
  var videoName = $.urlParam('videoName')
  var url = 'https://s3.us-west-2.amazonaws.com/syn-ai-aaoi/scenes/'+videoName+'/thumbnails/'+idScene+'.png'

  var container = document.getElementById('container')
  container.style.backgroundImage = "url('" + url + "')"; 

  AWS.config = new AWS.Config();
  AWS.config.accessKeyId = "AKIAIWDYUGA5XOAWBU7Q";
  AWS.config.secretAccessKey = "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";
  AWS.config.region = "us-west-2";
  var BucketName = "syn-ai-aaoi";
  var s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: {Bucket: BucketName}
  });

  $(document).on('click', '#crop', function(){
  
    const params = {
      Key: 'models/' + videoName + '/files/' + idScene + '.JSON', // file will be saved as testBucket/contacts.csv
      Body: JSON.stringify(Dots, null, 2)
    };

    s3.upload(params, function(err, data) {
      if (err) {
        return alert('There was an error uploading your file: ', err.message);
      }
      alert('File uploaded successfully');
    });

  });

  $(document).on('click', '#show', function(){
    var picture = "";
    var root = "https://s3.us-west-2.amazonaws.com/syn-ai-aaoi/models";
    picture += "<img class='mask' src='"+root+"/"+videoName+"/masks/"+idScene+".png'/>";
    picture += "<hr/>"
    picture += "<img class='crop' src='"+root+"/"+videoName+"/crops/"+idScene+".png'/>";
    document.getElementById("models").innerHTML = picture;
  
  });

  $(document).on('click', '#search', function(){
    window.open('http://localhost:1227/BoxedGallery/main.html?idScene='+idScene+'&videoName='+videoName,'_blank')
  });

});

$.urlParam = function (name) {
  var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
  return (results !== null) ? results[1] || 0 : false;
}
